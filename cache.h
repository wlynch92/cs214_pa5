#ifndef CACHE_H
#define CACHE_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "sorted-list.h"
#include "index.h"
#include "search.h"

typedef struct {
    char *token; /* word */
    fpos_t offset; /* offset in file */ 
} cacheNode;

/* Creates reference table of all the words that exist in filename.
 * returns pointer to allocated referencetable on success, else null.
 */
cacheNode* createRefTable(char* filename);

/* Search reference table for given target. Returns pointer to allocated 
 * SLNode of target on success, null on failure.
 */
SLNode* searchRefTable(char *target, cacheNode* table, char* filename);

/* Make SLNode based on information given by the reference table. Returns 
 * pointer to allocated SLNode on success, null on failure.
 */
SLNode* makeSLNode(int res, cacheNode* table, char* filename); 

/* Fill the initial cache with as many nodes as possible. Returns pointer to 
 * resulting index on success, null on failure.
 */
Index *buildCacheIndex(cacheNode *list, char *filename);

/* Inserts given node into the cache. Returns 1 if the insert is sucessful.
 * Otherwise returns 0 on failure.
 */
int cacheInsert(Index *indexer, SLNode *node);

/* Frees all memory allocated to the reference table */
void destroyRefTable(cacheNode* table);

/* Gets the size of a given SLNode and its sublist */
unsigned int getSLSize(SLNode* node);

/* Removes a node from the cache. Returns 1 on success, 0 if there are no more nodes to remove
 */
int evictNode(Index* indexer);

/* Given string of some size in kb/md/gb, returns size in bytes. 
 * Returns -1 if the input is not formatted properly
 */
long parseSize(char *str);

#endif
