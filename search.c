#include "search.h"
#define DEBUG 10

/*
 * CS214 PA4: search
 * Searching utility based on an indexed file.
 *
 * Written by:  Bill Lynch <wlynch92@eden.rutgers.edu>
 *              Jenny Shi  <jenny.shi@rutgers.edu>
 *
 */


/*
 * Search function to merge lists and print result output.
 */
void search(Index *indexer,char *args,int andor){
    char *tok;
    tok=strtok(args," ");
    NodeList *a=NULL, *b=NULL, *c;
    while (tok!=NULL){
        b=nodeListCreate(indexer,tok);
        if (b==NULL && andor!=0){
            nodeListDestroy(a);
            a=NULL;
            break;
        }
        c=nodeListMerge(a,b,andor);
        nodeListDestroy(a);
        nodeListDestroy(b);
        a=nodeListCopy(c);
        nodeListDestroy(c);
        tok=strtok(NULL," ");
    }

    c=a;
    /* prints out the result */
    while (c!=NULL){
        printf("%s\n",c->node); 
        c=c->next;
    }
    nodeListDestroy(a);
}

/*
 * Creates initial NodeList structure for the given word in the indexer.
 * Used to manipulate lists without changing underlying index struct.
 * Returns NodeList for given word on success.
 * Returns NULL if word does not exist in the index struct.
 */
NodeList *nodeListCreate(Index *indexer, char *word){
    NodeList *list=NULL;
    NodeList *tmp,*curr;
    int insert=1;
    SLNode *ptr=getFileList(indexer,word),*node=NULL;
    if (ptr==NULL){ /* check cache */
        node=searchRefTable(word,NULL,NULL);
        if (node!=NULL){
            ptr=node->fileSublist->head;
            insert=cacheInsert(indexer,node);
        }
    }
    if (ptr != NULL){
        NodeList *tmp = (NodeList *) malloc (sizeof(NodeList));
        char *ptrword=(char *)ptr->val;
        tmp->node=(char *)calloc(strlen(ptrword)+1,sizeof(char));
        strcpy(tmp->node,ptrword);
        tmp->next=NULL;
        list=tmp;
        curr=list;
        ptr=ptr->next;
    }

    while (ptr != NULL && curr != NULL){
        tmp = (NodeList *) malloc (sizeof(NodeList));
        char *ptrword=(char *)ptr->val;
        tmp->node=(char *)calloc(strlen(ptrword)+1,sizeof(char));
        strcpy(tmp->node,ptrword);
        tmp->next=NULL;
        curr->next=tmp;
        ptr=ptr->next;
        curr=curr->next;
    }
    if (insert==0){
        SLDestroy(node->fileSublist);
        free(node->val);
        free(node);
    }

    return list;
}

/*
 * Merges lists based on the given operation:
 * op=1 and
 * op=0 or
 * Returns new resulting merged list.
 * The calling user is responsible for freeing the resulting list.
 */
NodeList *nodeListMerge(NodeList *a, NodeList *b, int op){
    /* Some initial checks to see if either list is empty */
    if (a==NULL && b==NULL){
        return NULL;
    }
    if (a==NULL){
        return nodeListCopy(b);
    }
    if (b==NULL){
        return nodeListCopy(a);
    }

    NodeList *curr,*retval=NULL;
    if (op==0){
        /* Or:
         * 2 lists, A & B
         * if A[i] = B[i]: add A[i], move both
         * if A[i] < B[i]: add A[i], move A
         * if A[i] > B[i]: add B[i], move B
         * if either list is EOL, add rest of other list.
         * O(max(len(A,B)))
         */
        int first=0;
        NodeList *a_iter=a, *b_iter=b;
        /* Go through every node and build a new list */
        while (a_iter!=NULL && b_iter!=NULL){
            int cmp = strcmp(a_iter->node,b_iter->node);
            if (cmp==0){
                /* If equal, we can advance both nodes */
                NodeList *tmp = (NodeList *) malloc (sizeof(NodeList));
                tmp->node=(char *)calloc(strlen(a_iter->node)+1,sizeof(char));

                strcpy(tmp->node,a_iter->node);
                tmp->next=NULL;
                if (first==0){
                    retval=tmp;
                    curr=retval;
                    first=1;
                } else {
                    curr->next=tmp;
                    curr=curr->next;
                }
                a_iter=a_iter->next;
                b_iter=b_iter->next;
            } else if (cmp<0) {
                /* else a < b */
                NodeList *tmp = (NodeList *) malloc (sizeof(NodeList));
                tmp->node=(char *)calloc(strlen(a_iter->node)+1,sizeof(char));

                strcpy(tmp->node,a_iter->node);
                tmp->next=NULL;
                if (first==0){
                    retval=tmp;
                    curr=retval;
                    first=1;
                } else {
                    curr->next=tmp;
                    curr=curr->next;
                }
                a_iter=a_iter->next;
            } else {
                /* else b < a */
                NodeList *tmp = (NodeList *) malloc (sizeof(NodeList));
                tmp->node=(char *)calloc(strlen(b_iter->node)+1,sizeof(char));

                strcpy(tmp->node,b_iter->node);
                tmp->next=NULL;
                if (first==0){
                    retval=tmp;
                    curr=retval;
                    first=1;
                } else {
                    curr->next=tmp;
                    curr=curr->next;
                }
                b_iter=b_iter->next;
            }
        }

        /* Add the remainder of the non-empty list to retval */
        if (a_iter==NULL){
            curr->next=nodeListCopy(b_iter);
        } else {
            curr->next=nodeListCopy(a_iter);
        }
    } else {
        /* And:
         * 2 lists, A & B
         * if A[i] = B[i]: add A[i], move both
         * if A[i] < B[i]: move A
         * if A[i] > B[i]: move B
         * if either are EOL, done
         * O(min(len(A,B))) time
         */
        int first=0;
        NodeList *a_iter=a, *b_iter=b;
        while (a_iter!=NULL && b_iter!=NULL){
            int cmp = strcmp(a_iter->node,b_iter->node);
            if (cmp==0){
                NodeList *tmp = (NodeList *) malloc (sizeof(NodeList));
                tmp->node=(char *)calloc(strlen(a_iter->node)+1,sizeof(char));

                strcpy(tmp->node,a_iter->node);
                tmp->next=NULL;
                if (first==0){
                    retval=tmp;
                    curr=retval;
                    first=1;
                } else {
                    curr->next=tmp;
                    curr=curr->next;
                }
                a_iter=a_iter->next;
                b_iter=b_iter->next;
            } else if (cmp<0) {
                a_iter=a_iter->next;
            } else {
                b_iter=b_iter->next;
            }
        }

    }

    return retval;
}

/*
 * Makes a copy of list. The user is responsible for freeing the resulting list.
 * Returns a duplicate of list.
 */
NodeList *nodeListCopy(NodeList *list){
    NodeList *iter=list,*retval=NULL,*tmp,*curr;
    int first=0;
    while (iter!=NULL){
        tmp=(NodeList*) malloc (sizeof(NodeList));
        tmp->node=(char *)calloc(strlen(iter->node)+1,sizeof(char));
        strcpy(tmp->node,iter->node);
        tmp->next=NULL;
        if (first==0){
            retval=tmp;
            curr=retval;
            first=1;
        } else {
            curr->next=tmp;
            curr=curr->next;
        }
        iter=iter->next;
    }
    return retval;
}

/*
 * Destroys (frees) given NodeList)
 */
void nodeListDestroy(NodeList *list){
    NodeList *temp = list;
    NodeList *freetemp;

    while (temp != NULL) {
        if (temp->next == NULL) {
            free(temp->node);
            free(temp);
            break;
        }

        freetemp = temp;
        temp = freetemp->next;
        free(freetemp->node);
        free(freetemp);
    }

    return;
}

