Similar test cases from the previous assignment (pa4) were used for testing cache. 

The first part of our test cases were mainly concerned with the sa/so functionalities without a specified cache size, and while some internal structures were modified to accomodate the new caching feature, the results are identical to those from pa4. This is just to double check that everything stayed the same for normal searching.

The second part contains user inputted cache sizes, and tests how our programs handles the search function with a limited cache space. The main indexer file we used was test/results/fic.txt, where three very large files (Austen's "Persuasion", Bronte's "Jane Eyre", and Swift's "A Modest Proposal") were used to make the file through ./index--if our program works for this huge file, then we can safely assume that it will also run correctly for smaller files such as simple.txt.  

All of the test cases with include valgrind outputs to ensure that there are no memory leaks

The main areas tested for the sa/so seach functions are as below:
- no words
- one word
- two of the same words
- two words with/without common files
- more than two words
- non-existent words (no outputs)
- existent word + non-existent word (no output on sa, existent word's files on so)
- invalid command (anything other than so or sa)

The test indexer files can be found in the test/results directory; the actual text files (e.g. alice.txt) can be found in test/[directory name]. 

------------------PART ONE------------------

1. Incorrect parameters
[jingfei@basic:cs214_pa5]$ valgrind ./search hai.txt bai.txt
==18662== Memcheck, a memory error detector
==18662== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==18662== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==18662== Command: ./search hai.txt bai.txt
==18662==
Incorrect arguments given.
==18662==
==18662== HEAP SUMMARY:
==18662==     in use at exit: 0 bytes in 0 blocks
==18662==   total heap usage: 0 allocs, 0 frees, 0 bytes allocated
==18662==
==18662== All heap blocks were freed -- no leaks are possible
==18662==
==18662== For counts of detected and suppressed errors, rerun with: -v
==18662== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

2. No parameters
[jingfei@basic:cs214_pa5]$ valgrind ./search
==18680== Memcheck, a memory error detector
==18680== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==18680== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==18680== Command: ./search
==18680==
Incorrect arguments given.
==18680==
==18680== HEAP SUMMARY:
==18680==     in use at exit: 0 bytes in 0 blocks
==18680==   total heap usage: 0 allocs, 0 frees, 0 bytes allocated
==18680==
==18680== All heap blocks were freed -- no leaks are possible
==18680==
==18680== For counts of detected and suppressed errors, rerun with: -v
==18680== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

3. Non-existent file: hai.txt
[jingfei@basic:cs214_pa5]$ valgrind ./search hai.txt
==18640== Memcheck, a memory error detector
==18640== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==18640== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==18640== Command: ./search hai.txt
==18640==
file hai.txt does not exist. exiting.
==18640==
==18640== HEAP SUMMARY:
==18640==     in use at exit: 0 bytes in 0 blocks
==18640==   total heap usage: 1 allocs, 1 frees, 568 bytes allocated
==18640==
==18640== All heap blocks were freed -- no leaks are possible
==18640==
==18640== For counts of detected and suppressed errors, rerun with: -v
==18640== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

4. Simple file (test/results/simple.txt)
[jingfei@basic:cs214_pa5]$ valgrind ./search test/results/simple.txt
==18561== Memcheck, a memory error detector
==18561== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==18561== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==18561== Command: ./search test/results/simple.txt
==18561==
search> sa
search> so
search> sa a
test/simple/a
test/simple/abc.txt
search> sa a a
test/simple/a
test/simple/abc.txt
search> so b
test/simple/a
test/simple/abc.txt
search> so b b
test/simple/a
test/simple/abc.txt
search> sa a b
test/simple/a
test/simple/abc.txt
search> so a b
test/simple/a
test/simple/abc.txt
search> sa c jumped
search> so c jumped
test/simple/a
test/simple/abc.txt
search> so inflation fox
test/simple/a
search> sa inflation fox
search> sa inflation
search> so inflation
search> sa the quick were
test/simple/a
search> so lazy b c dog
test/simple/a
test/simple/abc.txt
search> sa lazy b c dog
test/simple/a
search> q
==18561==
==18561== HEAP SUMMARY:
==18561==     in use at exit: 0 bytes in 0 blocks
==18561==   total heap usage: 400 allocs, 400 frees, 36,281 bytes allocated
==18561==
==18561== All heap blocks were freed -- no leaks are possible
==18561==
==18561== For counts of detected and suppressed errors, rerun with: -v
==18561== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

5. Indexer file with many file sublists: (test/results/nested.txt)
[jingfei@basic:cs214_pa5]$ valgrind ./search test/results/nested.txt
==18742== Memcheck, a memory error detector
==18742== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==18742== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==18742== Command: ./search test/results/nested.txt
==18742==
search> sa two to
test/nested/phd.txt
search> so two to
test/nested/anotherdir/fox.txt
test/nested/anotherdir/onemoredir/anotherfox.txt
test/nested/crime.txt
test/nested/paperfromgrace.txt
test/nested/phd.txt
search> sa the theft
search> so the theft
test/nested/anotherdir/fox.txt
test/nested/anotherdir/onemoredir/anotherfox.txt
test/nested/crime.txt
test/nested/paperfromgrace.txt
test/nested/phd.txt
search> sa the to
test/nested/anotherdir/fox.txt
test/nested/anotherdir/onemoredir/anotherfox.txt
test/nested/paperfromgrace.txt
test/nested/phd.txt
search> sa the to well
test/nested/paperfromgrace.txt
search> sa the to well of
test/nested/paperfromgrace.txt
search> sa deflation inflation
search> so deflation inflation
search> q
==18742==
==18742== HEAP SUMMARY:
==18742==     in use at exit: 0 bytes in 0 blocks
==18742==   total heap usage: 1,344 allocs, 1,344 frees, 332,759 bytes allocated
==18742==
==18742== All heap blocks were freed -- no leaks are possible
==18742==
==18742== For counts of detected and suppressed errors, rerun with: -v
==18742== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

6. Large file (test/results/large.txt)
[jingfei@basic:cs214_pa5]$ valgrind ./search test/results/large.txt
==18798== Memcheck, a memory error detector
==18798== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==18798== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==18798== Command: ./search test/results/large.txt
==18798==
search> add
Unrecognized command
search> sa common coming
test/large/alice.txt
search> so common coming
test/large/alice.txt
test/large/wow.txt
search> sa calamity calculated
test/large/wow.txt
search> so calamity calculated
test/large/wow.txt
search> sa calamity calculated busied cake
search> so calamity calculated busied cake
test/large/alice.txt
test/large/wow.txt
search> sa nonexistent1 nonexistent2 nonexistent3
search> so buttered ne1
test/large/alice.txt
search> sa buttered ne1
search> q
==18798==
==18798== HEAP SUMMARY:
==18798==     in use at exit: 0 bytes in 0 blocks
==18798==   total heap usage: 10,107 allocs, 10,107 frees, 2,286,735 bytes allocated
==18798==
==18798== All heap blocks were freed -- no leaks are possible
==18798==
==18798== For counts of detected and suppressed errors, rerun with: -v
==18798== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

------------------PART TWO------------------

1. Preliminary test without specified cache size (test/results/fic.txt)
[jingfei@basic:cs214_pa5]$ valgrind ./search test/results/fic.txt 
==26467== Memcheck, a memory error detector
==26467== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==26467== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==26467== Command: ./search test/results/fic.txt
==26467== 
search> sa blinds blister
test/fic/austen_persuasion.txt
search> so blinds blister
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa body
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so bohemian
test/fic/jane_eyre
search> sa born country
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so invite investigation
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa miss mortals
search> so miss mortals
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa plan hai
search> so plan hai
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa reprobates reprobates
test/fic/austen_persuasion.txt
search> so scraps scraps
test/fic/jane_eyre
test/fic/swift_modest.txt
search> q
==26467== 
==26467== HEAP SUMMARY:
==26467==     in use at exit: 0 bytes in 0 blocks
==26467==   total heap usage: 130,393 allocs, 130,393 frees, 33,368,429 bytes allocated
==26467== 
==26467== All heap blocks were freed -- no leaks are possible
==26467== 
==26467== For counts of detected and suppressed errors, rerun with: -v
==26467== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

2. cache size: 1GB (test/results/fic.txt)
[jingfei@basic:cs214_pa5]$ valgrind ./search -m 1GB test/results/fic.txt 
==19047== Memcheck, a memory error detector
==19047== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==19047== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==19047== Command: ./search -m 1GB test/results/fic.txt
==19047== 
search> sa blinds blister
test/fic/austen_persuasion.txt
search> so blinds blister
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa body
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so bohemian
test/fic/jane_eyre
search> sa born country
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so invite investigation
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa miss mortals
search> so miss mortals
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa plan hai
search> so plan hai
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa reprobates reprobates
test/fic/austen_persuasion.txt
search> so scraps scraps
test/fic/jane_eyre
test/fic/swift_modest.txt
search> q
==19047== 
==19047== HEAP SUMMARY:
==19047==     in use at exit: 0 bytes in 0 blocks
==19047==   total heap usage: 129,410 allocs, 129,410 frees, 59,880,645 bytes allocated
==19047== 
==19047== All heap blocks were freed -- no leaks are possible
==19047== 
==19047== For counts of detected and suppressed errors, rerun with: -v
==19047== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

3. cache size: 64MB (test/results/fic.txt)
[jingfei@basic:cs214_pa5]$ valgrind ./search -m 64MB test/results/fic.txt 
==27333== Memcheck, a memory error detector
==27333== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==27333== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==27333== Command: ./search -m 64MB test/results/fic.txt
==27333== 
search> sa blinds blister
test/fic/austen_persuasion.txt
search> so blinds blister
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa body
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so bohemian
test/fic/jane_eyre
search> sa born country
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so invite investigation
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa miss mortals
search> so miss mortals
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa plan hai
search> so plan hai
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa reprobates reprobates
test/fic/austen_persuasion.txt
search> so scraps scraps
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa stateliest stated starve
test/fic/jane_eyre
search> so stateliest stated starve
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa torrent torn
test/fic/jane_eyre
search> sa torrent torn torture
test/fic/jane_eyre
search> so torrent torn torture
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> q
==27333== 
==27333== HEAP SUMMARY:
==27333==     in use at exit: 0 bytes in 0 blocks
==27333==   total heap usage: 129,521 allocs, 129,521 frees, 59,882,717 bytes allocated
==27333== 
==27333== All heap blocks were freed -- no leaks are possible
==27333== 
==27333== For counts of detected and suppressed errors, rerun with: -v
==27333== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

4. cache size: 128KB (test/results/fic.txt)
[jingfei@basic:cs214_pa5]$ valgrind ./search -m 128KB test/results/fic.txt 
==7649== Memcheck, a memory error detector
==7649== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==7649== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==7649== Command: ./search -m 128KB test/results/fic.txt
==7649== 
search> sa blinds blister
test/fic/austen_persuasion.txt
search> so blinds blister
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa body
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so bohemian
test/fic/jane_eyre
search> sa born country
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so invite investigation
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa miss mortals
search> so miss mortals
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa plan hai
search> so plan hai
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa reprobates reprobates
test/fic/austen_persuasion.txt
search> so scraps scraps
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa stateliest stated starve
test/fic/jane_eyre
search> so stateliest stated starve
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa torrent torn torture
test/fic/jane_eyre
search> so torrent torn torture
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> q
==7649== 
==7649== HEAP SUMMARY:
==7649==     in use at exit: 0 bytes in 0 blocks
==7649==   total heap usage: 15,190 allocs, 15,190 frees, 15,169,543 bytes allocated
==7649== 
==7649== All heap blocks were freed -- no leaks are possible
==7649== 
==7649== For counts of detected and suppressed errors, rerun with: -v
==7649== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

5. cache size: 16KB (test/results/fic.txt)
[jingfei@basic:cs214_pa5]$ valgrind ./search -m 16kb test/results/fic.txt 
==14478== Memcheck, a memory error detector
==14478== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==14478== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==14478== Command: ./search -m 16kb test/results/fic.txt
==14478== 
search> sa blinds blister         
test/fic/austen_persuasion.txt
search> so blinds blister 
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa body
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so bohemian
test/fic/jane_eyre
search> sa born country
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so invite investigation
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa miss mortals
search> so miss mortals
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa plan hai
search> so plan hai
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa reprobates reprobates
test/fic/austen_persuasion.txt
search> so scraps scraps
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa stateliest stated starve
test/fic/jane_eyre
search> so stateliest stated starve
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa torrent torn torture
test/fic/jane_eyre
search> so torrent torn torture
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> q
==14478== 
==14478== HEAP SUMMARY:
==14478==     in use at exit: 0 bytes in 0 blocks
==14478==   total heap usage: 14,828 allocs, 14,828 frees, 15,027,019 bytes allocated
==14478== 
==14478== All heap blocks were freed -- no leaks are possible
==14478== 
==14478== For counts of detected and suppressed errors, rerun with: -v
==14478== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

6. cache size: 1KB (test/results/fic.txt)
[jingfei@basic:cs214_pa5]$ valgrind ./search -m 1kb test/results/fic.txt 
==20304== Memcheck, a memory error detector
==20304== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==20304== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==20304== Command: ./search -m 1kb test/results/fic.txt
==20304== 
search> sa blinds blister
test/fic/austen_persuasion.txt
search> so blinds blister
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa body
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so bohemian
test/fic/jane_eyre
search> sa born country
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so invite investigation
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa miss mortals
search> so miss mortals
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa plan hai
search> so plan hai
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa reprobates reprobates
test/fic/austen_persuasion.txt
search> so scraps scraps
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa stateliest stated starve
test/fic/jane_eyre
search> so stateliest stated starve
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa torrent torn torture
test/fic/jane_eyre
search> so torrent torn torture
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> q
==20304== 
==20304== HEAP SUMMARY:
==20304==     in use at exit: 0 bytes in 0 blocks
==20304==   total heap usage: 14,878 allocs, 14,878 frees, 15,046,359 bytes allocated
==20304== 
==20304== All heap blocks were freed -- no leaks are possible
==20304== 
==20304== For counts of detected and suppressed errors, rerun with: -v
==20304== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)

7. cache size: 0kb (test/results/fic.txt)
[jingfei@basic:cs214_pa5]$ valgrind ./search -m 0kb test/results/fic.txt 
==22796== Memcheck, a memory error detector
==22796== Copyright (C) 2002-2010, and GNU GPL'd, by Julian Seward et al.
==22796== Using Valgrind-3.6.0 and LibVEX; rerun with -h for copyright info
==22796== Command: ./search -m 0kb test/results/fic.txt
==22796== 
search> sa blinds blister
test/fic/austen_persuasion.txt
search> so blinds blister
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa body
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so bohemian
test/fic/jane_eyre
search> sa born country
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> so invite investigation
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa miss mortal
test/fic/jane_eyre
search> sa miss mortals
search> so miss mortals
test/fic/austen_persuasion.txt
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa plan hai
search> so plan hai
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> sa reprobates reprobates
test/fic/austen_persuasion.txt
search> so scraps scraps
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa stateliest stated starve
test/fic/jane_eyre
search> so stateliest stated starve
test/fic/jane_eyre
test/fic/swift_modest.txt
search> sa torrent torn torture
test/fic/jane_eyre
search> so torrent torn torture
test/fic/austen_persuasion.txt
test/fic/jane_eyre
search> q
==22796== 
==22796== HEAP SUMMARY:
==22796==     in use at exit: 0 bytes in 0 blocks
==22796==   total heap usage: 144,752 allocs, 144,752 frees, 48,296,778 bytes allocated
==22796== 
==22796== All heap blocks were freed -- no leaks are possible
==22796== 
==22796== For counts of detected and suppressed errors, rerun with: -v
==22796== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 6 from 6)
