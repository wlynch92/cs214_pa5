#include "cache.h"

/* CS214 PA5: Cached Search
 * Written by:
 *              Bill Lynch  <wlynch92@eden.rutgers.edu>
 *              Jenny Shi   <jenny.shi@rutgers.edu>
 */

long cache_size; /* user specified cache size */
int cache_node_count=0; /* number of tokens in indexer file */
char *cache_filename=NULL; /* indexer file */
cacheNode *reftable=NULL; /* table of tokens and offset */

cacheNode* createRefTable(char* filename) {

    int i, j, slot = 0;
    char line[2048];
    char list[8];
    char startlist[8] = "<list> ";

    FILE *file = fopen(filename, "r");
    if (file != NULL) {
        /* count number of tokens for exact size array */
        while (fgets(line, 2048, file) != NULL) {
            strncpy(list, line, sizeof(list));
            list[7] = '\0';
            if (strcmp(startlist, list) == 0) cache_node_count++;
        }
        rewind(file);

        /* create reference table */
        cacheNode *ref = (cacheNode*)malloc(cache_node_count*sizeof(cacheNode));
        
        /* fills reference table */
        while (fgets(line, 2048, file) != NULL) {
            strncpy(list, line, sizeof(list));
            list[7] = '\0';
            if (strcmp(startlist, list) == 0) {
                
                /* allocate mem for each word found in indexer file
                 * word size is 1024 for consistency 
                 */
                char *word = (char*)malloc(sizeof(char)*1024); 
                for (i = 0, j = 7; j < strlen(line)-1; i++, j++) {
                    word[i] = line[j];
                }
                word[i] = '\0';
                ref[slot].token = word; /* ref->token ptr points to word in mem */
                fgetpos(file, &(ref[slot].offset)); /* stores offset pos */
                slot++;
            }
        }
        fclose(file);
        return ref;
    }
    else { 
        /* if the file does not exist, return NULL, main will handle 
         * and close */
        fprintf(stderr, "File %s cannot be opened, exiting.\n", filename);
        return NULL;
    }
}

SLNode* searchRefTable(char *target, cacheNode* table, char* filename) {

    if (table==NULL){
        table=reftable;
    }
    if (filename==NULL){
        filename=cache_filename;
    }
    int i=0;

    for (; i < cache_node_count; i++) {
        if (strcmp(table[i].token, target) == 0) {
            SLNode *result = makeSLNode(i, table, filename);
            return result;
        }
    }

    return NULL;
}

SLNode* makeSLNode(int res, cacheNode* table, char* filename) {

    int k=0, l=0, freq;
    char line[2048];
    char c;
   
    /* allocate mem for SLNode token as it is not shareable 
     * with the malloc'ed word in cacheNode 
     */
    char *tok = (char*)malloc(sizeof(char)*1024);
    strcpy(tok, table[res].token);

    /* make new token node */
    SLNode *token = (SLNode*)malloc(sizeof(SLNode));
    token->val = tok;
    token->next = NULL;
    token->ptrcount = 0;
    token->count = 0;
    token->fileSublist = SLCreate(compareStrings, 1, 1);

    FILE *file = fopen(filename, "r");
    fsetpos(file, &(table[res].offset));
    
    if(fgets(line, 2048, file) != NULL) {

        while(k < strlen(line)-1) {
            c = line[k];
            char *fname = (char*)calloc(1024, sizeof(char));
            char *num = (char*)calloc(50, sizeof(char));

            /* get filename */
            while (c != ' ') {
                fname[l] = c;
                l++;
                if (k < strlen(line)-1) {
                    k++;
                    c = line[k];
                }
                else break;
            }
            fname[l] = '\0';
            l = 0;
            k++;

            /* get count */
            c = line[k];
            while (c != ' ') {
                num[l] = c;
                l++;
                if (k < strlen(line)-1) {
                    k++;
                    c = line[k];
                }
                else break;
            }
            num[l] = '\0';
            freq = atoi(num);
            l=0;

            if (k < strlen(line)) k++;
            
            /* new file name node */
            SLNode *fnode = (SLNode*)malloc(sizeof(SLNode));
            fnode->val = fname;
            fnode->next = NULL;
            fnode->ptrcount = 0;
            fnode->count = freq;             
            fnode->fileSublist = NULL;
            SLNodeInsert(token->fileSublist, fnode);
           
            free(num);
        }
    }

    fclose(file);
    return token;
}

Index *buildCacheIndex(cacheNode *list,char *filename){
    if (list==NULL || filename==NULL){
        return NULL;
    }
    int i;
    Index* indexer=indexerCreate();
    for (i=0; i<cache_node_count; i++){
        SLNode* node=makeSLNode(i,list,filename);
        int node_size=getSLSize(node);
        if (node_size<cache_size){
            cache_size-=node_size;
            SLNodeInsert(indexer->tokenlist,node);
        }else{ /* free */
            SLDestroy(node->fileSublist);
            free(node->val);
            free(node);
            break;
        }

    }
    return indexer;
}

/* inserts node into cache. returns 0 on failure, non zero on success */
int cacheInsert(Index* indexer, SLNode* target){
    SLNode *node=target;
    if (node->fileSublist==NULL){ 
        //fprintf(stderr,"NULL\n");
    }
    int node_size=getSLSize(node);
    while(1){
        if (node_size<cache_size){
            cache_size-=node_size;
            SLNodeInsert(indexer->tokenlist,node);
            return 1;
        }else{
            /* the cache is too small */
            if (evictNode(indexer)==0){ 
                return 0;
            }
        }
    }
}

void destroyRefTable(cacheNode* table) {

    int i = 0;
    /* goes through ref table and frees each slot */
    for (; i < cache_node_count; i++) {
        free((table+i)->token); 
    }

    free(table); /* frees reference table */
}

unsigned int getSLSize(SLNode* node){
    int retval=1056;
    if (node->fileSublist!=NULL){
        retval+=28+(1056*node->fileSublist->count);
    }
    return retval;
}

/* Takes in a node, evicts a random node in the list */
int evictNode(Index* indexer){
    if (indexer->tokenlist->count==0){
        return 0;
    }
    srandom(time(NULL));
    int loc=random()%indexer->tokenlist->count;
    SLNode* ptr=indexer->tokenlist->head;
    int i;
    for (i=0; i<loc-1; i++){
        ptr=ptr->next;
    }
    if (ptr==NULL){
        //fprintf(stderr,"ptr is null\n");
    }
    char *word=(char *)ptr->val;
    unsigned int size=getSLSize(ptr);
    if (SLRemove(indexer->tokenlist,word)!=0){
        cache_size+=size; 
    }
    return 1;
}

/* Takes in a string in the form x[kb/mb/gb]. 
 * Returns the size of cache, -1 on failure */
long parseSize(char *str){
    long retval=-1;
    if (strcmp(str+(strlen(str)-2),"KB")==0 || strcmp(str+(strlen(str)-2),"kb")==0){
        char *tok=strtok(str,"kK");
        int i=0;
        for (; i<strlen(tok);i++){
            if (isdigit(tok[i])==0){
                return retval;
            }
        }
        retval=atol(tok)*1024;
    } else if (strcmp(str+(strlen(str)-2),"MB")==0 || strcmp(str+(strlen(str)-2),"mb")==0){
        char *tok=strtok(str,"mM");
        int i=0;
        for (; i<strlen(tok);i++){
            if (isdigit(tok[i])==0){
                return retval;
            }
        }
        retval=atol(tok)*1024*1024;
    } else if (strcmp(str+(strlen(str)-2),"GB")==0 || strcmp(str+(strlen(str)-2),"gb")==0){
        char *tok=strtok(str,"gG");
        int i=0;
        for (; i<strlen(tok);i++){
            if (isdigit(tok[i])==0){
                return retval;
            }
        }
        retval=atol(tok)*1024*1024*1024;
    }
    return retval;
}

int main(int argc, char* argv[]) {
    if (argc == 4){
        if (strcmp(argv[1],"-m")!=0){
            printf("Incorrect arguments given.\n");
            return 1;
        }
        long size=parseSize(argv[2]);
        if (size < 0){
            printf("Invalid size\n");
            return 2;
        }
        cache_filename=argv[3];
        reftable = createRefTable(argv[3]);
        cache_size=size;
    } else if (argc == 2){
        cache_filename=argv[1];
    } else {
        printf("Incorrect arguments given.\n");
        return 1;
    }

    /* Search main starts here */


    /* We need some sort of way to only insert an initial amount of nodes
     * into the indexer in the beginning.
     */
    char *args,*cmd,*input=NULL;
    size_t size=0;
    
    Index *indexer;
    if (cache_size==0){
        indexer = indexerFileCreate(cache_filename);
    } else {
        indexer = buildCacheIndex(reftable, cache_filename);
    }

    if (indexer != NULL){
        do{
            /* Prompt and input */
            printf("search> ");
            getline(&input,&size,stdin);
            /* Remove trailing \n char */
            cmd=strtok(input,"\n");
            /* if input is empty, try again */
            if (cmd==NULL){
                continue;
            }

            if (strncmp(cmd,"sa ",3)==0){
                /* Isolate arguments */
                args = (char *) malloc ((strlen(cmd)-2)*sizeof(char));
                strcpy(args,cmd+3);
                strcat(args,"\0");
                search(indexer,args,1);
                free(args);
            } else if (strncmp(cmd,"so ",3)==0){
                /* Isolate arguments */
                args = (char *) malloc ((strlen(cmd)-2)*sizeof(char));
                strcpy(args,cmd+3);
                strcat(args,"\0");
                search(indexer,args,0);
                free(args);
            } else if (strcmp(cmd,"q")==0) {
                free(cmd);
                break;
            } else if (strcmp(cmd,"sa")!=0 && strcmp(cmd,"so")!=0){
                printf("Unrecognized command\n");
            }
        } while(1);

        destroyRefTable(reftable);
        indexerDestroy(indexer); 
    } else {
        printf("file %s does not exist. exiting.\n",argv[1]);
        exit(2);
    }
    return 0;
}

