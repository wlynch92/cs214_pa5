#ifndef SEARCH_H
#define SEARCH_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "index.h"
#include "cache.h"

struct NodeList{
    char *node;
    struct NodeList* next;
};
typedef struct NodeList NodeList;

void search(Index *indexer, char *args, int andor);
NodeList *nodeListCreate(Index *indexer, char *word);
NodeList *nodeListMerge(NodeList *a, NodeList *b, int op);
NodeList *nodeListCopy(NodeList *list);
void nodeListDestroy(NodeList *list);

#endif
