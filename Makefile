CFLAGS= -Wall -g

#all: search index cache

all:index cache

cache: libsl index.o search.o cache.o
	gcc $(CFLAGS) cache.o index.o search.o -o search -L. -lsl

cache.o: cache.c cache.h libsl
	gcc $(CFLAGS) -c cache.c

index: libsl index.o index-main.o
	gcc $(CFLAGS) index-main.o index.o -o index -L. -lsl

index-main.o: index-main.c
	gcc $(CFLAGS) -c index-main.c

index.o: index.c index.h libsl
	gcc $(CFLAGS) -c index.c 

libsl: sorted-list.o 
	ar rcs libsl.a sorted-list.o

search.o: search.c search.h
	gcc $(CFLAGS) -c search.c 

sorted-list.o: sorted-list.c sorted-list.h
	gcc $(CFLAGS) -c sorted-list.c

clean:
	rm *.o libsl.a index search 
